<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cassis extends Model
{
    protected $table = 'cassis';
    use HasFactory;
}
